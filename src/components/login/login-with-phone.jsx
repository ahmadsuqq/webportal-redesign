"use client";
import FormGenerator from "@/components/shared/form-generator";
import { usePPDB } from "@/context/ppdb.context";
import { useRequestOtp } from "@/hooks/auth.hooks";
import { Box, Button, Container, Heading, Text, useToast } from "@chakra-ui/react";
import { useFormik } from "formik";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import * as yup from "yup";

export default function LoginWithPhoneNumber({ setIsUsingEmail }) {
  const toast = useToast();
  const { create, status, errorMessage } = useRequestOtp();
  const router = useRouter();
  const formik = useFormik({
    initialValues: {
      phone: "",
      nisn: "",
    },
    onSubmit: async (val) => {
      await create({
        phone: `62${val.phone}`,
        password: val.nisn,
        otpWith: "phone",
      });
      router.push("/verify-otp");
    },
    validationSchema: yup.object().shape({
      phone: yup.string().required("Masukan no telepon anda"),
      nisn: yup.string().required("Masukan nisn anda"),
    }),
  });
  const dataForm = [
    {
      id: "phone",
      label: "No Hp",
      type: "number",
      placeholder: "No Hp",
      leftAddon: "+62",
      // colSpan: 1,
    },
    {
      id: "nisn",
      label: "Password",
      type: "password",
      placeholder: "Password",
      // colSpan: 1,
    },
  ];

  useEffect(() => {}, []);

  return (
    <>
      <Container maxW="100%" height="100%" display="flex" flexDirection="column" justifyContent="center" bgColor="rgba(190, 227, 248, 0.5)" rounded={8} gap="56px">
        <Heading textAlign="center">Login Melalui Ezzi School</Heading>
        <Container display="flex" flexDir="column" gap="39px">
          <Box>
            <FormGenerator dataForm={dataForm} formik={formik} grid={2} id={"form-generator"} />
            <Box textAlign="center" mt="16px">
              <Text
                cursor="pointer"
                textDecoration="underline"
                fontSize={"14px"}
                color={"rgba(26, 32, 44, 0.70)"}
                onClick={async () => {
                  // router.push("/login/with_email");
                  setIsUsingEmail(false);
                  toast({
                    title: "Login menggunkan email",
                    duration: 1000,
                    status: "info",
                  });
                }}
              >
                Masuk Melalui Email
              </Text>
            </Box>
          </Box>
          <Box display="flex" justifyContent="center" alignItems="center">
            <Button
              bgColor="#2C5282"
              color="white"
              _hover="none"
              type="submit"
              form="form-generator"
              // onClick={() => formik.handleSubmit()}
              isLoading={status == "fetching"}
              loadingText={"Loading"}
            >
              Masuk
            </Button>
          </Box>
        </Container>
      </Container>
    </>
  );
}
